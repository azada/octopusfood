//
//  Restaurant.swift
//  OctopusFood
//
//  Created by Ahmed Zada on 16/07/2018.
//  Copyright © 2018 Ahmed Zada. All rights reserved.
//

import Foundation

class Restaurant {
    
    var outletName: String?
    var rating: Int?
    
    init(outletName: String, rating: Int) {
        self.outletName = outletName
        self.rating = rating
    }  
}
