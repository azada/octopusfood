//
//  ViewController.swift
//  OctopusFood
//
//  Created by Ahmed Zada on 15/07/2018.
//  Copyright © 2018 Ahmed Zada. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation


class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var popUpImg: UIImageView!
    @IBOutlet weak var outletNameLbl: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var cardsStackView: UIStackView!
    @IBOutlet weak var congratulationsView: UIView!
    @IBOutlet weak var discountMessageView: UIView!
    @IBOutlet weak var discountBtn: UIButton!
    @IBOutlet weak var refreshStackView: UIStackView!
    
    
    @IBOutlet var ratingStars: [UIImageView]!
    @IBOutlet var cardsBtns: [UIButton]!
    
    @IBOutlet var labels: [UILabel]!
    
    var cardToFoodNumber = [Int:Int]()
    var matched = false
    var firstCardSelected:UIButton?
    var secondCardSelected:UIButton?
    var matchedCounter = 0
    var restaurantsList:[Restaurant]?
    
    let locationManager = CLLocationManager()
    var latLon: String = "51.4622020,-2.6180660" //default value
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scaleFont(labels: labels)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        
        cardsStackView.isHidden = true
        refreshStackView.isHidden = true
        discountBtn.isHidden = true
        discountBtn.isUserInteractionEnabled = false
        assignImages()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        //Authorizing user location use
        locationAuthStatus()
        
        //downloading top 16 restaurants in 1 mile vicinity using Google Places
        downloadRestaurant {
            restaurants in
            self.restaurantsList = restaurants
            self.cardsStackView.isHidden = false
            self.discountBtn.isHidden = false
            self.refreshStackView.isHidden = false
        }
    }
    
    
    //assigning food images to cards in a random basis
    func assignImages() {
        
        var cards = [Int](0...15)
        var numbers = [Int](1...16)
     
        for _ in 1...8 {
            
            let foodIndex = Int(arc4random_uniform(UInt32(numbers.count)))
            let foodNumber = numbers[foodIndex]
            let foodImage = UIImage(named: "Food\(foodNumber)")
            
            for _ in 1...2 {
                let cardIndex = Int(arc4random_uniform(UInt32(cards.count)))
                let cardNumber = cards[cardIndex]
                
                if let image = foodImage {
                    cardsBtns[cardNumber].setImage(image, for: .selected)
                    cards.remove(at: cardIndex)
                    cardToFoodNumber[cardsBtns[cardNumber].tag] = foodNumber
                }
                
            }
            numbers.remove(at: foodIndex)
        }
    }
    
    
    //Location authorization
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            if let loc = locationManager.location {
                latLon = "\(loc.coordinate.latitude),\(loc.coordinate.longitude)"
            } else {
                locationManager.requestWhenInUseAuthorization()
                locationAuthStatus()
            }
        } else {
            locationManager.requestWhenInUseAuthorization()
            locationAuthStatus()
        }
    }
    
    
    //Downloading all restaurants in 1 mile vicinity and picking the highest rated 16 restaurants
    func downloadRestaurant(completed: @escaping ([Restaurant]) -> Void) {
        
        let url = "\(URL_BASE)location=\(latLon)&radius=\(RADIUS)&type=\(TYPE)&key=\(API_KEY)"
        
        Alamofire.request(url).responseJSON { (response) in
            
            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                
                if let results = dict["results"] as? [Dictionary<String, AnyObject>] {
                    
                    var myArray = results
                    
                    myArray.sort{
                        (($0 )["rating"] as! Double) > (($1 )["rating"] as! Double)
                    }
                    
                    var restaurants = [Restaurant]()
                    for i in 0...15 {
                        let restaurant = Restaurant(outletName: myArray[i]["name"] as! String, rating: Int(myArray[i]["rating"] as! Double))
                        
                        restaurants.append(restaurant)
                    }
                    completed(restaurants)
                }
            }
        }
    }
    
    
    //Animation and pop up message when cards match
    func cardsMatch() {
        
        UIView.transition(with: self.congratulationsView as UIView, duration: 1, options: .transitionCurlUp, animations: {
            self.congratulationsView.isHidden = false
        }) { (completed) in
            
            
            UIView.transition(with: self.congratulationsView as UIView, duration: 1, options: .transitionCurlDown , animations: {
                self.congratulationsView.isHidden = true
            }, completion: nil)
        }
        
        firstCardSelected?.isHidden = true
        secondCardSelected?.isHidden = true
    }
    
    
    //A function to scale font size based on screen size
    func scaleFont (labels:[UILabel]) {
        
        for label in labels {
            
            let attText = label.attributedText as! NSMutableAttributedString
            label.attributedText = NSAttributedString()
            
            attText.enumerateAttribute(
                NSAttributedStringKey.font,
                in: NSRange(location: 0, length: (attText.length)),
                options: [.longestEffectiveRangeNotRequired]
            ) { value, range, stop in
                
                if let font = value as! UIFont? {
                    
                    let newFont = font.withSize(font.pointSize * AppDelegate.screenRatio)
                    attText.removeAttribute(NSAttributedStringKey.font, range: range)
                    attText.addAttribute(NSAttributedStringKey.font, value: newFont, range: range)
                }
            }
            let labelFont = UIFont(name: label.font.fontName, size: label.font.pointSize * AppDelegate.screenRatio)
            if let labelFont = labelFont {
                label.font = labelFont
            }
            label.attributedText = attText
            
        }
    }
    
    
    @IBAction func cardSelected(_ sender: UIButton) {
        
        if !sender.isSelected {
            
            sender.isUserInteractionEnabled = false
            cardsStackView.isUserInteractionEnabled = false
            refreshStackView.isUserInteractionEnabled = false
            
            UIView.transition(with: sender as UIView, duration: 1.5, options: .transitionFlipFromRight, animations: {
                sender.isSelected = true
            }) { (completed) in
                
                //assign image, outlet/restaurant name and rating to pop up view
                if let foodNumber = self.cardToFoodNumber[sender.tag] {
                    if let image = UIImage(named: "Food\(foodNumber)") {
                        self.popUpImg.image = image
                        if let restaurants = self.restaurantsList {
                            if let outletName = restaurants[sender.tag-1].outletName {
                                self.outletNameLbl.text = outletName
                            }
                            if let outletRating = restaurants[sender.tag-1].rating {
                                for star in self.ratingStars {
                                    if star.tag <= outletRating {
                                        star.image = UIImage(named: "Star Yellow")
                                    } else {
                                        star.image = UIImage(named: "Star White")
                                    }
                                }
                            }
                        }
                    }
                }
                
                UIView.transition(with: self.popUpView as UIView, duration: 0.75, options: .transitionCrossDissolve, animations: {
                    self.popUpView.isHidden = false
                }, completion: nil)
            }
            
            if firstCardSelected == nil {
                firstCardSelected = sender
            } else if secondCardSelected == nil {
                secondCardSelected = sender
                
                if let firstCardTag = firstCardSelected?.tag, let secondCardTag = secondCardSelected?.tag {
                    if cardToFoodNumber[firstCardTag] == cardToFoodNumber[secondCardTag]{
                        matched = true
                        matchedCounter += 1
                    }
                }
            }
        }
    }
    
    
    @IBAction func closePressed(_ sender: UIButton) {

        self.popUpView.isHidden = true
        
        if self.matched {
            matched = false
            cardsMatch()
            firstCardSelected = nil
            secondCardSelected = nil
            
            if matchedCounter == 8 {
                cardsStackView.isHidden = true
                discountBtn.isUserInteractionEnabled = true
                
                //pulse animation
                let pulse1 = CASpringAnimation(keyPath: "transform.scale")
                pulse1.duration = 0.6
                pulse1.fromValue = 1.0
                pulse1.toValue = 1.12
                pulse1.autoreverses = true
                pulse1.repeatCount = 1
                pulse1.initialVelocity = 0.5
                pulse1.damping = 0.8
                
                let animationGroup = CAAnimationGroup()
                animationGroup.duration = 2.7
                animationGroup.repeatCount = 1000
                animationGroup.animations = [pulse1]
                
                discountBtn.layer.add(animationGroup, forKey: "pulse")
            }
  
        } else {
            if secondCardSelected != nil {
                
                UIView.transition(with: self.firstCardSelected! as UIView, duration: 0.75, options: .transitionFlipFromLeft, animations: {
                    self.firstCardSelected?.isSelected = false
                }, completion: nil)
                
                UIView.transition(with: self.secondCardSelected! as UIView, duration: 0.75, options: .transitionFlipFromLeft, animations: {
                    self.secondCardSelected?.isSelected = false
                }, completion: nil)
                
                firstCardSelected?.isUserInteractionEnabled = true
                secondCardSelected?.isUserInteractionEnabled = true
                
                firstCardSelected = nil
                secondCardSelected = nil
            }
        }
        cardsStackView.isUserInteractionEnabled = true
        refreshStackView.isUserInteractionEnabled = true
    }

    
    @IBAction func discountBtnPressed(_ sender: UIButton) {
        
        discountMessageView.isHidden = false
        refreshStackView.isUserInteractionEnabled = false
        discountBtn.layer.removeAllAnimations()
    }
    
    
    @IBAction func discountClosePressed(_ sender: UIButton) {
        
        discountMessageView.isHidden = true
        refreshStackView.isUserInteractionEnabled = true
    }
    
    
    @IBAction func refreshPressed(_ sender: Any) {

        cardToFoodNumber = [:]
        matched = false
        firstCardSelected = nil
        secondCardSelected = nil
        matchedCounter = 0
        cardsStackView.isHidden = false
        congratulationsView.isHidden = true
        discountMessageView.isHidden = true
        for card in cardsBtns {
            card.isHidden = false
            card.isSelected = false
            card.isUserInteractionEnabled = true
        }
        discountBtn.isUserInteractionEnabled = false
        discountBtn.layer.removeAllAnimations()
        assignImages()
    }
}
