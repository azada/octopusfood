//
//  Constants.swift
//  OctopusFood
//
//  Created by Ahmed Zada on 15/07/2018.
//  Copyright © 2018 Ahmed Zada. All rights reserved.
//

import Foundation

let URL_BASE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
let LOCATION = "51.4622020,-2.6180660"
let RADIUS = "1609" //1 mile
let API_KEY = "AIzaSyBpRST3UOOSzbh8ykoIhH-j3HhOKFb3isg"
let TYPE = "restaurant"
let url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=51.4622020,-2.6180660&radius=1609&type=restaurant&key=AIzaSyBpRST3UOOSzbh8ykoIhH-j3HhOKFb3isg"

typealias DownloadComplete = () -> ()
